from django.shortcuts import render
from django.contrib.auth.decorators import login_required

from contact.models import Contact


def _ctx_user_contacts(user):
    contacts = Contact.objects.filter(user=user)
    return {'contacts': contacts}


@login_required
def profile(request):

    ctx = {
            **_ctx_user_contacts(request.user),
            'user': request.user,
        }
    return render(request, 'user/profile.html', ctx)
