from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

import page.views
import secure.views
import res.views
import user.views
import contact.views


urlpatterns = [

    path('', page.views.home, name='homepage'),
    path('p/about', page.views.about, name='page-about'),

    path('login/', secure.views.login, name='login'),
    path('login/token/sent/', secure.views.login_token_sent, name='login-token-sent'),
    path('login/<str:token>/', secure.views.login_token, name='login-token'),
    path('login/token/failed/', secure.views.login_token_failed, name='login-token-failed'),
    path('logout/', secure.views.logout, name='logout'),

    path('r/new/', res.views.send, name='res-send'),
    path('r/send/', res.views.save, name='res-save'),
    path('r/send/success/', res.views.send_success, name='res-send-success'),
    path('r/sent/', res.views.sent, name='res-sent'),
    path('r/received/', res.views.received, name='res-received'),
    path('r/<int:pk>/mark-as-seen/', res.views.mark_as_seen, name='res-mark-as-seen'),

    path('u/profile/', user.views.profile, name='user-profile'),

    path('contact/refresh/', contact.views.refresh, name='contact-refresh'),

]



if settings.DEBUG == True:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
