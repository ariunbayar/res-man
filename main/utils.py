from authlib.jose import jwt
from urllib import request

from django.conf import settings
from django.core import mail


def send_mail(subject, body_plain, recipients):

    if settings.JWT_TO_SMTP_APP:

        payload = {
                'emails': [
                        {
                            'recipients': recipients,
                            'subject': subject,
                            'body_plain': body_plain,
                        },
                    ]
            }

        try:
            message = jwt.encode(settings.JWT['headers'], payload, settings.JWT['key'])
            content = request.urlopen(settings.JWT_TO_SMTP_APP, message).read()
            claims = jwt.decode(content, key)
        except:
            return False
        else:
            return True

    else:

        num_sent = mail.send_mail(subject, body_plain, settings.EMAIL_FROM, recipients, fail_silently=False)
        return num_sent
