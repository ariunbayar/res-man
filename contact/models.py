from django.db import models
from django.conf import settings
from django.utils.functional import cached_property


class Contact(models.Model):

    email = models.CharField(max_length=250)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT)
