from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required

from res.models import Res

from .models import Contact


@login_required
def refresh(request):

    emails_existing = Contact.objects.filter(user=request.user).values_list('email', flat=True)

    emails_received = Res.objects.filter(email_from=request.user.email).values_list('email_to', flat=True)
    emails_sent = Res.objects.filter(email_to=request.user.email).values_list('email_from', flat=True)

    emails = set(list(emails_received) + list(emails_sent)) - set(emails_existing)

    contacts = (Contact(email=email, user=request.user) for email in emails)
    Contact.objects.bulk_create(contacts)

    return redirect('user-profile')
