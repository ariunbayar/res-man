from django.shortcuts import render, redirect, reverse, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse

from .models import Res, Image, Audio, Video, Attachment
from .forms import ResSendForm


@login_required
def received(request):

    res_list = Res.objects.filter(email_to=request.user.email)

    ctx = {
            'res_list': res_list,
        }
    return render(request, 'res/received_list.html', ctx)


@login_required
def sent(request):

    res_list = Res.objects.filter(email_from=request.user.email)

    ctx = {
            'res_list': res_list,
        }
    return render(request, 'res/sent_list.html', ctx)


@login_required
def send(request):

    form = ResSendForm(request.user)

    ctx = {
            'form': form,
        }
    return render(request, 'res/send.html', ctx)


@login_required
def save(request):

    is_success = False

    form = ResSendForm(request.user, request.POST)
    if form.is_valid():

        res = Res.objects.create(
                email_from=request.user.email,
                email_to=form.cleaned_data.get('email_to'),
                summary=form.cleaned_data.get('summary'),
                status=Res.STATUS_NEW,
            )

        for image in request.FILES.getlist('images'):
            Image.objects.create(res=res, filename=image)

        audio = request.FILES.get('audio')
        if audio:
            Audio.objects.create(res=res, filename=audio)

        video = request.FILES.get('video')
        if video:
            Video.objects.create(res=res, filename=video)

        audios = request.FILES.getlist('audios')
        if audios:
            for audio in audios:
                Audio.objects.create(res=res, filename=audio)

        attachments = request.FILES.getlist('attachments')
        if attachments:
            for attachment in attachments:
                Attachment.objects.create(res=res, filename=attachment, name=attachment.name)

        is_success = True

    ctx = {
            'success': is_success,
            'errors': form.errors,
            'redirect': reverse('res-send-success'),
        }
    return JsonResponse(ctx)


@login_required
def send_success(request):
    return render(request, 'res/send_success.html', {})


@login_required
def mark_as_seen(request, pk):
    Res.objects.filter(pk=pk, email_to=request.user.email).update(status=Res.STATUS_SEEN)
    return redirect('res-received')
