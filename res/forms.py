from django import forms
from django.core.validators import EmailValidator

from contact.models import Contact


default_error_messages = {
    'invalid': "Зөв оруулна уу!",
    'required': "оруулна уу!",
    'max_length': "%(limit_value)d-с илүүгүй урттай оруулна уу!",
    'min_length': "%(limit_value)d-с багагүй урттай оруулна уу!",
}


class ResSendForm(forms.Form):

    contact = forms.ChoiceField(
            error_messages={**default_error_messages},
            required=False,
        )

    email_to = forms.CharField(
            label='Мэйл хаяг',
            max_length=254,
            validators=[EmailValidator()],
            error_messages={**default_error_messages, 'invalid': "Мэйл хаягийг зөв оруулна уу!"},
            widget=forms.EmailInput,
            required=False,
        )

    summary = forms.CharField(
            label='Агуулга',
            min_length=2,
            required=True,
            error_messages={**default_error_messages},
            widget=forms.Textarea,
        )

    def get_contact_choices(self, user):
        options = [(c.pk, c.email) for c in Contact.objects.filter(user=user)]
        return [('', '---------')] + options

    def __init__(self, user, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.fields['contact'].choices = self.get_contact_choices(user)

    def clean(self):

        super().clean()

        email_to = self.cleaned_data.get('email_to')
        contact_id = self.cleaned_data.get('contact')

        if not email_to and not contact_id:
            if not self.has_error('email_to'):
                self.add_error('email_to', default_error_messages['required'])
        else:
            if contact_id:
                contact = Contact.objects.filter(pk=contact_id).first()
                self.cleaned_data['email_to'] = contact.email
