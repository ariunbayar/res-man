from django.db import models
from django.utils.functional import cached_property


class Res(models.Model):

    class Meta:

        ordering = ['-created_at']

    STATUS_NEW = 'new'
    STATUS_SEEN = 'seen'

    STATUS_CHOICES = [
            (STATUS_NEW, 'шинэ'),
            (STATUS_SEEN, 'үзсэн'),
        ]

    summary = models.TextField()
    email_from = models.CharField(max_length=250)
    email_to = models.CharField(max_length=250)
    status = models.CharField(max_length=20, choices=STATUS_CHOICES, default='new')

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @property
    def images(self):
        return self.image_set.all()

    @cached_property
    def audio(self):
        return self.audio_set.all().first()

    @cached_property
    def video(self):
        return self.video_set.all().first()

    @cached_property
    def audios(self):
        return self.audio_set.all()

    @cached_property
    def attachments(self):
        return self.attachment_set.all()


class Image(models.Model):

    res = models.ForeignKey(Res, on_delete=models.PROTECT)
    filename = models.ImageField(upload_to='images/%Y/%m/%d/', height_field='height', width_field='width')
    width = models.PositiveIntegerField()
    height = models.PositiveIntegerField()


class Audio(models.Model):
    res = models.ForeignKey(Res, on_delete=models.PROTECT)
    filename = models.FileField(upload_to='%Y/%m/%d/audio/')


class Video(models.Model):
    res = models.ForeignKey(Res, on_delete=models.PROTECT)
    filename = models.FileField(upload_to='%Y/%m/%d/video/')


class Attachment(models.Model):
    res = models.ForeignKey(Res, on_delete=models.PROTECT)
    name = models.CharField(max_length=250)
    filename = models.FileField(upload_to='attachment/%Y/%m/%d/')
