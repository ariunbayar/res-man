(function(){
    document.querySelectorAll('.audio-preview audio').forEach((el) => preload(el));

    function preload(el){

        let url = el.getAttribute('data-src');

        el.style.display = 'none';

        var xhr = new XMLHttpRequest();
        xhr.open("GET", url, true);
        xhr.responseType = 'blob';
        xhr.onreadystatechange = function() {
            if (this.readyState == 4) {
                if (this.status == 200) {
                    el.style.display = '';
                    el.src = URL.createObjectURL(this.response);
                }
            }
        };
        xhr.send();

    }
})();
