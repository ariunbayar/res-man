window.Utils = (function () {

    function Utils(){

    };


    Utils.prototype.toFormData = function toFormData(data) {
        let form_data = new FormData();
        for (let key in data) {
            form_data.append(key, data[key]);
        }
        return form_data;
    }

    Utils.prototype.xhr_post = function xhr_post(data, url, success_fn, error_fn) {
        let form_data = this.toFormData(data);
        this.xhr_post1(form_data, url, success_fn, error_fn);
    }

    Utils.prototype.xhr_post1 = function xhr_post1(form_data, url, success_fn, error_fn) {
        var xhr = new XMLHttpRequest();
        xhr.open("POST", url, true);
        xhr.onreadystatechange = function() {
            if (this.readyState == 4) {

                if (this.status == 200) {
                    let data;
                    try {
                        data = JSON.parse(xhr.responseText);
                    } catch (error) {
                        data = null;
                    }

                    success_fn(data);
                } else {
                    error_fn(xhr);
                }
            }
        };
        xhr.send(form_data);
    }

    Utils.prototype.escapeHtml = function escapeHtml(unsafe) {
        return unsafe
            .replace(/&/g, "&amp;")
            .replace(/</g, "&lt;")
            .replace(/>/g, "&gt;")
            .replace(/"/g, "&quot;")
            .replace(/'/g, "&#039;");
    }

    Utils.prototype.domEscapedText = function domEscapedText(text) {
        let textEscaped = this.escapeHtml(text);
        return document.createTextNode(textEscaped);
    }


    return new Utils();

})();
