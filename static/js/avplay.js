(function(){


    function AVPreview(container) {

        this.container = container;
        this.preview = null;
        this.hidden = true;

        this.btn = container.querySelector('.play-btn');
        this.audio = container.querySelector('audio');
        this.video = container.querySelector('video');

        this.btn.addEventListener('click', this.play.bind(this));
        this.audio.addEventListener('timeupdate', this.audioTimeUpdated.bind(this));

    }


    AVPreview.prototype.getPreview = function getPreview() {

        let preview;

        if (this.preview !== null) {
            return this.preview;
        }

        preview = document.querySelector('#fullscreen-av-preview-auto');
        if (preview) {
            this.preview = preview;
            return this.preview;
        }


        // Create preview

        preview = document.createElement('div');
        document.body.appendChild(preview);

        preview.style.backgroundColor = 'rgba(0, 0, 0, 0.35)';
        preview.style.position = 'fixed';
        preview.style.top = '0px';
        preview.style.left = '0px';
        preview.style.bottom = '0px';
        preview.style.right = '0px';
        preview.style.display = 'none';
        preview.style.padding = '20px';
        preview.addEventListener('click', this.hide.bind(this));

        this.preview = preview;

        return preview;

    }


    AVPreview.prototype.show = function show() {
        let preview = this.getPreview();
        preview.style.display = 'block';
        this.hidden = false;
        this.popOutVideo();
    }


    AVPreview.prototype.hide = function hide() {
        this.pause();
        let preview = this.getPreview();
        preview.style.display = 'none';
        this.hidden = true;
        this.popInVideo();
    }


    AVPreview.prototype.popOutVideo = function popOutVideo() {
        let video = this.video;

        let orig_video_attrs = {};
        let attrs = [
            'width', 'height', 'max-width', 'max-height', 'position', 'borderRadius', 'border',
            'top', 'left', 'zIndex', 'transform'
        ];

        attrs.forEach(function(attr){
            orig_video_attrs[attr] = video.style[attr];
        });

        video.style.width = '';
        video.style.height = '';
        video.style.maxWidth = 'calc(100vw - 40px)';
        video.style.maxHeight = 'calc(100vh - 40px)';
        video.style.position = 'fixed';
        video.style.top = '50%';
        video.style.left = '50%';
        video.style.zIndex = '1000';
        video.style.transform = 'translate(-50%, -50%)';

        this.popInVideo = function(){
            attrs.forEach(function(attr){
                video.style[attr] = orig_video_attrs[attr];
            });
        }
    }


    AVPreview.prototype.play = function play() {

        this.show();

        let video = this.video;
        let audio = this.audio;

        if (!video.paused) video.pause();
        if (!audio.paused) audio.pause();
        video.currentTime = 0;
        audio.currentTime = 0;
        video.play();
        audio.play();
    }

    AVPreview.prototype.pause = function pause() {
        if (!this.video.paused) this.video.pause();
        if (!this.audio.paused) this.audio.pause();
    }

    AVPreview.prototype.audioTimeUpdated = function audioTimeUpdated(e) {
        let diff_sec = Math.abs(this.video.currentTime - this.audio.currentTime);
        if (diff_sec > 0.5) {
            this.video.currentTime = this.audio.currentTime;
        }
    }


    document.querySelectorAll('.av-preview').forEach(function(preview){
        new AVPreview(preview);
    });




})();
