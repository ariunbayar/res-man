window.AVRec = (function(){

    'use strict';


    function Visualizer(canvas, stream) {

        this.canvas = canvas;
        this.ctx = canvas.getContext('2d');

        this.width = canvas.width;
        this.height = canvas.height;

        this.is_running = false;

        this.start(canvas, stream);

    }


    Visualizer.prototype.start = function start(canvas, stream) {

        this.is_running = true;

        let audioCtx = new (window.AudioContext || window.webkitAudioContext)();
        let analyser = audioCtx.createAnalyser();
        let source = audioCtx.createMediaStreamSource(stream);
        source.connect(analyser);
        analyser.fftSize = 256;
        let bufferLength = analyser.frequencyBinCount;
        let dataArray = new Uint8Array(bufferLength);
        this.ctx.clearRect(0, 0, this.width, this.height);

        this.analyser = analyser;
        this.dataArray = dataArray;
        this.bufferLength = bufferLength;

        this.draw();

    }


    Visualizer.prototype.draw = function draw() {

        if (!this.is_running) return;

        requestAnimationFrame(this.draw.bind(this));

        let w = this.width;
        let h = this.height;
        let ctx = this.ctx;
        let dataArray = this.dataArray;

        this.analyser.getByteFrequencyData(dataArray);

        ctx.clearRect(0, 0, w, h);

        let barWidth = (w / dataArray.length) * 2.5;
        let x = 0;
        dataArray.forEach(function(barHeight, i){
            ctx.fillStyle = 'rgb(' + (255 - barHeight) + ',255,' + (255 - barHeight) + ')';
            ctx.fillRect(x, h - barHeight / 4, barWidth, barHeight);
            x += barWidth + 1;
        });
    }


    Visualizer.prototype.stop = function stop() {
        this.is_running = false;
    }


    function AVRec(container) {

        this.container = container;

        this.el_video = this.container.querySelector('.video');
        this.el_audio_vis = this.container.querySelector('canvas.audio-vis');
        this.el_preview = this.container.querySelector('.avrec-preview');

        this.btn_start = this.container.querySelector('.record-start');
        this.btn_stop = this.container.querySelector('.record-stop');

        this.is_recording = 0b00;

        this.btn_start.addEventListener('click', this.start.bind(this));
        this.btn_stop.addEventListener('click', this.stop.bind(this));
        this.reset();

    }


    AVRec.prototype.reset = function reset() {
        this.recording = {audio: [], video: []};
        this.stream = {audio: null, video: null};
        this.recorder = {audio: null, video: null};
    }


    AVRec.prototype.start = async function start() {

        this.stream.video = await this.getScreenStream();
        this.stream.audio = await this.getAudioStream();

        this.el_video.srcObject = this.stream.video;

        this.el_preview.style.display = 'block';

        this.record();
    }


    AVRec.prototype.stop = function stop() {
        if (this.is_recording) {
            this.recorder.video.stop();
            this.recorder.audio.stop();
            this.el_preview.style.display = 'none';
        }
    }


    AVRec.prototype.getScreenStream = async function getScreenStream() {
        if (navigator.getDisplayMedia) {
            return await navigator.getDisplayMedia({video: true});
        } else if (navigator.mediaDevices.getDisplayMedia) {
            return await navigator.mediaDevices.getDisplayMedia({video: true});
        } else {
            return await navigator.mediaDevices.getUserMedia({video: {mediaSource: 'screen'}});
        }
    }

    AVRec.prototype.getAudioStream = async function getAudioStream() {
        return await navigator.mediaDevices.getUserMedia({audio: true});
    }


    AVRec.prototype.record = function record() {

        let video_options = {mimeType: 'video/webm;codecs=vp9'};
        if (!MediaRecorder.isTypeSupported(video_options.mimeType)) {
            video_options = {mimeType: 'video/webm;codecs=vp8'};
            if (!MediaRecorder.isTypeSupported(video_options.mimeType)) {
                video_options = {mimeType: 'video/webm'};
                if (!MediaRecorder.isTypeSupported(video_options.mimeType)) {
                    video_options = {mimeType: ''};
                }
            }
        }

        this.recorder.video = new MediaRecorder(this.stream.video, video_options);
        this.recorder.video.addEventListener('start', () => this.is_recording |= 0b10);
        this.recorder.video.addEventListener('stop', () => this.is_recording &= 0b01);
        this.recorder.video.addEventListener('dataavailable', this.storeData.bind(this, 'video'));
        this.recorder.video.start(100); // collect 100ms of data each

        this.recorder.audio = new MediaRecorder(this.stream.audio);
        this.recorder.audio.addEventListener('start', () => this.is_recording |= 0b01);
        this.recorder.audio.addEventListener('stop', () => this.is_recording &= 0b10);
        this.recorder.audio.addEventListener('dataavailable', this.storeData.bind(this, 'audio'));
        this.recorder.audio.start(100); // collect 100ms of data each

        this.visualizer = new Visualizer(this.el_audio_vis, this.stream.audio);

    }


    AVRec.prototype.storeData = function storeData(datatype, event) {
        if (event.data) {
            this.recording[datatype].push(event.data);
            let msg = document.querySelector('#errorMsg');
        }
    }

    AVRec.prototype.hasRecording = function hasRecording() {
        return this.recording.audio.length > 0;
    }


    AVRec.prototype.getRecordedAudio = function getRecordedAudio() {
        this.stop();
        let blobs = this.recording.audio;
        return new Blob(blobs, {type: blobs[0].type});
    }


    AVRec.prototype.getRecordedVideo = function getRecordedVideo() {
        let blobs = this.recording.video;
        return new Blob(blobs, {type: blobs[0].type});
    }

    return AVRec;

})();
