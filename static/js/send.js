(function(){

    var submit_images = [];
    var submit_audios = [];
    var submit_attachments = [];
    var avrec;

    var el_avrec = document.querySelector('.avrec');
    avrec = new AVRec(el_avrec);

    var el = document.querySelector('textarea');
    el.addEventListener('paste', function(e){
        let items = event.clipboardData.items;
        for (index in items) {
            let item = items[index];
            if (item.kind === 'file') {
                readFile(item.getAsFile());
            }
        }
    });

    function readFile(f) {

        let reader = new FileReader();

        reader.onload = function(event){

            if (event.target.result.startsWith('data:')) {

                let b64type = event.target.result.split(';', 1)[0];

                if (audio_types.indexOf(b64type) > -1) {
                    addAudio(f, event.target.result);
                } else if (image_types.indexOf(b64type) > -1) {
                    addImage(f, event.target.result);
                } else {
                    addAttachment(f);
                }
            }

        };
        reader.readAsDataURL(f);

    }


    let image_types = ['data:image/png', 'data:image/jpeg'];
    let audio_types = ['data:audio/mp3', 'data:audio/wav'];
    el.addEventListener('drop', function(e){
        e.preventDefault();

        let dt = e.dataTransfer;
        let files = dt.files;

        ([...files]).forEach((f) => readFile(f));
    });

    var image_container = document.querySelector('.images');
    function addImage(blob, b64data) {
        submit_images.push(blob);
        let img = document.createElement('img');
        image_container.appendChild(img);
        img.src = b64data;
        img.style.maxWidth = '100px';
        img.style.maxHeight = '100px';
        img.style.borderRadius = '4px';
        img.style.border = '4px solid #fff';
    }

    var audio_container = document.querySelector('.audio-container');
    function addAudio(blob, b64data) {
        submit_audios.push(blob);
        let el = document.createElement('audio');
        audio_container.appendChild(el);
        el.controls = true;
        el.src = b64data;
    }

    var attachment_container = document.querySelector('.attachments');
    function addAttachment(blob) {
        submit_attachments.push(blob);
        let el = document.createElement('li');
        attachment_container.appendChild(el);
        el.appendChild(document.createTextNode(blob.name));
    }

    var form = document.querySelector('#send-form');

    form.addEventListener('submit', function(e) {

        let popup = document.querySelector('#popup-loading');
        popup.classList.remove('hidden');

        e.preventDefault();

        let form_data = Utils.toFormData({
            summary: form.querySelector('[name="summary"]').value,
            email_to: form.querySelector('[name="email_to"]').value,
            contact: form.querySelector('[name="contact"]').value,
            csrfmiddlewaretoken: form.querySelector('[name="csrfmiddlewaretoken"]').value,
        });
        submit_images.forEach(function(file){
            form_data.append('images', file);
        });
        submit_audios.forEach(function(file){
            form_data.append('audios', file);
        });
        submit_attachments.forEach(function(file){
            form_data.append('attachments', file);
        });
        if (avrec.hasRecording()) {
            form_data.append('audio', avrec.getRecordedAudio());
            form_data.append('video', avrec.getRecordedVideo());
        }

        // Cleanup current errors
        form.querySelectorAll('[id$="_error"]').forEach((el) => el.innerHTML = '');


        Utils.xhr_post1(form_data, form.action, function(rsp){
            if (rsp.success == true) {
                window.location = rsp.redirect;
            } else {
                popup.classList.add('hidden');
                for (let name in rsp.errors) {
                    let el_error = form.querySelector('#id_' + name + '_error');
                    if (!el_error) continue;
                    el_error.appendChild(Utils.domEscapedText(rsp.errors[name][0]));
                }
            }
        }, function(xhr){
            popup.classList.add('hidden');
            console.log('submit error');
        });

    });

    form.querySelector('[name="email_to"]').focus();

})();
