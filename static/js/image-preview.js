(function(){

    function ImagePreview(container) {

        this.container = container;
        this.preview = null;
        this.preview_image = null;
        this.hidden = true;

        this.setupImages(container);

    }

    ImagePreview.prototype.setupImages = function setupImages(container) {

        var image = null;

        container.querySelectorAll('img').forEach(function(img) {
            let _image = {prev: image, img: img, next: null};

            if (_image.prev !== null) {
                _image.prev.next = _image;
            }
            image = _image;

            img.addEventListener('click', this.onclick.bind(this, _image));
        }.bind(this));
    }


    ImagePreview.prototype.getPreview = function getPreview() {

        let preview, img;

        if (this.preview !== null) {
            return this.preview;
        }

        preview = document.querySelector('#fullscreen-image-preview-auto');
        if (preview) {
            this.preview = preview;
            return this.preview;
        }

        // Create preview

        preview = document.createElement('div');
        document.body.appendChild(preview);

        preview.style.backgroundColor = 'rgba(0, 0, 0, 0.35)';
        preview.style.position = 'fixed';
        preview.style.top = '0px';
        preview.style.left = '0px';
        preview.style.bottom = '0px';
        preview.style.right = '0px';
        preview.style.display = 'none';
        preview.style.padding = '20px';
        preview.addEventListener('click', this.hide.bind(this));

        // Create image inside preview

        img = document.createElement('img');
        preview.appendChild(img);
        img.style.maxHeight = '100%';
        img.style.margin = '0 auto';
        img.style.display = 'block';
        img.style.boxShadow = '0 0 0 4px #fff';
        img.style.borderRadius = '1px';

        this.preview = preview;
        this.preview_image = img;

        return preview;

    }

    ImagePreview.prototype.onclick = function onclick(image) {

        this.show();
        this.preview_image.src = image.img.src;

    }


    ImagePreview.prototype.show = function show() {
        let preview = this.getPreview();
        preview.style.display = 'block';
        this.hidden = false;
    }

    ImagePreview.prototype.hide = function hide() {
        let preview = this.getPreview();
        preview.style.display = 'none';
        this.hidden = true;
    }


    document.querySelectorAll('.image-preview').forEach(function(preview){
        new ImagePreview(preview);
    });


})();
