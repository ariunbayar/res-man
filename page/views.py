from django.shortcuts import render


def home(request):
    ctx = {}
    return render(request, 'page/home.html', ctx)


def about(request):
    ctx = {}
    return render(request, 'page/about.html', ctx)
